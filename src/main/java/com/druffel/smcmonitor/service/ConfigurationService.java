/*
 * Project:		monitor
 * Package:		com.druffel.smcmonitor.service
 * File: 		ConfigurationService.java
 *
 * Created:		Mar 7, 2017
 * Author:		AmonDruffel (Sophos Technology GmbH)
 * Copyright:	(C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.druffel.smcmonitor.entity.Configuration;

@Stateless
public class ConfigurationService extends AbstractService<Configuration>
{

    @PersistenceContext(unitName = "monitor")
    private EntityManager em;
    
    public ConfigurationService()
    {
        super(Configuration.class);
    }

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }
    
    public Configuration getConfigurationByName(String param)
    {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<Configuration> cq = cb.createQuery(Configuration.class);  
        Root<Configuration> r = cq.from(Configuration.class);
        cq.select(r);
        cq.where(cb.equal(r.get("key"), param));
        return getEntityManager().createQuery(cq).getSingleResult();
    }

}
