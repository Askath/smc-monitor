/*
 * Project:		monitor
 * Package:		com.druffel.smcmonitor.service
 * File: 		UpdateService.java
 *
 * Created:		Mar 9, 2017
 * Author:		AmonDruffel (Sophos Technology GmbH)
 * Copyright:	(C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.druffel.smcmonitor.entity.Update;

@Stateless
public class UpdateService extends AbstractService<Update>
{

    @PersistenceContext(unitName = "monitor")
    private EntityManager em;
    
    public UpdateService()
    {
        super(Update.class);
    }

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }
    
    public void clearUpdates(int count)
    {
        
        Query query = getEntityManager().createQuery("delete from Update").setMaxResults(count);
        query.executeUpdate();
        
    }
    
    public Update getLastUpdate()
    {
        List<Update> updateList = findAll();
        return updateList.get(updateList.size()-1);
    }

}
