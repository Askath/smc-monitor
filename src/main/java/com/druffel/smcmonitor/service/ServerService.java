/*
 * Project: monitor Package: com.druffel.smcmonitor.service File:
 * ServerService.java Created: Mar 2, 2017 Author: AmonDruffel (Sophos
 * Technology GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.druffel.smcmonitor.entity.Server;

@Stateless
public class ServerService extends AbstractService<Server>
{

    public ServerService()
    {
        super(Server.class);
    }

    @PersistenceContext(unitName="monitor")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager()
    {
        return em;

    }

}
