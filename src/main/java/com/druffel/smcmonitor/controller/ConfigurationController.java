/*
 * Project:		monitor
 * Package:		com.druffel.smcmonitor.controller
 * File: 		ConfigurationController.java
 *
 * Created:		May 16, 2017
 * Author:		AmonDruffel (Sophos Technology GmbH)
 * Copyright:	(C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.controller;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.druffel.smcmonitor.entity.Server;
import com.druffel.smcmonitor.service.ServerService;

@Dependent
public class ConfigurationController
{
    
    @Inject
    ServerService serverService;
    
    public void persistServerToDatabase(String url, String ip, String hostname, String description, boolean useUrl)
    {
        Server s = new Server();
        if(!useUrl)
        {
            s.setUrl(url);
        }else
        {
            s.setIp(ip);
        }
        s.setHostname(hostname);
        s.setStatus(false);
        s.setDescription(description);
        persistNewServer(s);
    }
    
    protected void persistNewServer(Server s)
    {
        serverService.create(s);
    }
    
    public List<Server> getAllServer()
    {
        return serverService.findAll();
    }
    
    public void mergeServer(Server s)
    {
        serverService.merge(s);
    }
    
}
