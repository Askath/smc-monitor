/*
 * Project: monitor Package: com.druffel.smcmonitor.controller File:
 * OverviewController.java Created: May 16, 2017 Author: AmonDruffel (Sophos
 * Technology GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.controller;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.net.ssl.TrustManagerFactory;

import com.druffel.smcmonitor.certificate.CertificateChecker;
import com.druffel.smcmonitor.entity.Server;
import com.druffel.smcmonitor.entity.Update;
import com.druffel.smcmonitor.network.ServerChecker;
import com.druffel.smcmonitor.rest.response.LoginResponse;
import com.druffel.smcmonitor.service.ServerService;
import com.druffel.smcmonitor.service.UpdateService;

@Dependent
public class OverviewController
{

    @Inject
    private ServerService serverService;

    @Inject
    private UpdateService updateService;

    @Inject
    private CertificateChecker certc;

    @Inject
    private ServerChecker serverChecker;

    private String truststoreName;

    private String truststorePassword;

    @PostConstruct
    public void init()
    {
        truststoreName = "truststore";
        truststorePassword = "truststorepassword";
    }

    public List<Server> refreshServerInformation(List<Server> serverList, String username, String password, String customer) throws Exception
    {
        List<Server> updatedServer = checkConnections(serverList);
        updatedServer = checkServers(updatedServer, username, password, customer);
        saveUpdateToDatabase(new Update(new Date()));
        return updatedServer;
    }

    protected List<Server> checkConnections(List<Server> serverList)
    {
        List<Server> newServer = new ArrayList<>();

        for (Server s : serverList)
        {
            if (isReachable(s))
            {
                s.setStatus(true);
                newServer.add(s);

            }
            else
            {
                s.setStatus(false);
                newServer.add(s);
            }
            mergeServer(s);
        }
        return newServer;
    }

    protected boolean isReachable(Server server)
    {

        String serverString = getServerString(server);

        try (Socket socket = new Socket())
        {
            socket.connect(new InetSocketAddress(serverString, 80), 500);
            return true;
        }
        catch (IOException e)
        {
            try (Socket socket = new Socket())
            {
                socket.connect(new InetSocketAddress(serverString, 443), 500);
                return true;
            }
            catch (IOException er)
            {
                return false;
            }
        }
    }

    public List<Server> checkServers(List<Server> serverList, String username, String password, String customer) throws Exception
    {
        List<Server> newServerList = new ArrayList<>();

        for (Server s : serverList)
        {
            if (s.isStatus())
            {

                if (!checkForCertificate(getServerString(s), truststoreName, truststorePassword))
                {

                    addCertificateToTruststore(getServerString(s));
                }

                TrustManagerFactory tmf = getTrustManagerFactory();
                tmf.init(getKeyStore());

                LoginResponse response = getLoginResponse(getServerString(s), username, password, customer);
                if (response != null)
                {
                    String version = getVersion(getServerString(s), response);

                    s.setLastknownversion(version);
                    newServerList.add(s);
                }
                else
                {
                    if (s.getLastknownversion() == null)
                    {
                        s.setLastknownversion("Unbekannt");
                    }
                    s.setLastknownversion(s.getLastknownversion());
                    newServerList.add(s);

                }

            }
            else
            {
                if (s.getLastknownversion() == null)
                {
                    s.setLastknownversion("Unbekannt");
                }
                s.setLastknownversion(s.getLastknownversion());
                newServerList.add(s);
            }
            serverService.merge(s);
            System.out.println("merged server");
        }
        return getAllServer();
    }

    protected String getVersion(String connectionUrl, LoginResponse response)
    {
        return serverChecker.getVersion(connectionUrl, response);
    }

    protected LoginResponse getLoginResponse(String connectionUrl, String username, String password, String customer)
    {
        return serverChecker.getLoginResponse(connectionUrl, username, password, customer);
    }

    protected KeyStore getKeyStore() throws Exception
    {
        return certc.loadCurrentTrustStore(truststoreName, truststorePassword);
    }

    protected TrustManagerFactory getTrustManagerFactory() throws NoSuchAlgorithmException
    {
        return TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    }

    protected void addCertificateToTruststore(String serverString) throws Exception
    {
        certc.addCertificateToTrustStore(serverString);
    }

    protected boolean checkForCertificate(String alias, String trustStoreName, String trustStorePasswordName) throws Exception
    {
        return certc.checkIfCertificateForAddressExists(alias, trustStoreName, trustStorePasswordName);
    }

    public int getLastRefreshInMinutes()
    {
        long sec = (new Date().getTime() - getLastUpdate().getUpdated().getTime()) / 1000;
        long minutes = sec / 60;
        return (int) minutes;
    }

    protected String getServerString(Server server)
    {
        return (server.getIp() == null || server.getIp().length() < 3) ? server.getUrl() : server.getIp();
    }

    protected Update getLastUpdate()
    {
        return updateService.getLastUpdate();
    }

    public void saveUpdateToDatabase(Update update)
    {
        updateService.create(update);
    }

    protected void persistNewServer(Server s)
    {
        serverService.create(s);
    }

    public List<Server> getAllServer()
    {
        return serverService.findAll();
    }

    public void mergeServer(Server s)
    {
        serverService.merge(s);
    }

    public String getTruststoreName()
    {
        return truststoreName;
    }

    public void setTruststoreName(String truststoreName)
    {
        this.truststoreName = truststoreName;
    }

    public String getTruststorePassword()
    {
        return truststorePassword;
    }

    public void setTruststorePassword(String truststorePassword)
    {
        this.truststorePassword = truststorePassword;
    }

}
