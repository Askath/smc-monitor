/*
 * Project: monitor Package: com.druffel.smcmonitor.certificate File:
 * CertificateChecker.java Created: May 16, 2017 Author: AmonDruffel (Sophos
 * Technology GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.certificate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.primefaces.util.Base64;

import com.druffel.smcmonitor.service.ConfigurationService;

@Dependent
public class CertificateChecker
{

    @Inject
    ConfigurationService configurationService;

    @Inject
    CertImporter certi;

    public void addCertificateToTrustStore(String serverString) throws Exception
    {
        certi.importCertificateIntoTruststore(serverString, serverString);
    }

    public boolean checkIfCertificateForAddressExists(String alias, String trustStoreName, String trustStorePasswordName) throws Exception
    {
        if (checkIfTrustStoreExists(trustStoreName))
        {
            createTrustStore();
        }

        
        return (loadCurrentTrustStore(trustStoreName, trustStorePasswordName).getCertificate(alias) == null) ? false : true;
    }

    public KeyStore loadCurrentTrustStore(String trustStoreName, String trustStorePassword) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
    {
        KeyStore sslTrustStore = KeyStore.getInstance("PKCS12");
        byte[] decodedKeystore = Base64.decode(configurationService.getConfigurationByName(trustStoreName).getValue().getBytes());
        sslTrustStore.load(new ByteArrayInputStream(decodedKeystore), trustStorePassword.toCharArray());
    
        return sslTrustStore;
    }

    protected InputStream getInputStreamFromTrustStore(String trustStoreName)
    {
        return new ByteArrayInputStream(Base64.decode(configurationService.getConfigurationByName(trustStoreName).getValue().getBytes()));
    }

    protected void createTrustStore() throws Exception
    {
        certi.createNewTrustStore();
    }

    protected boolean checkIfTrustStoreExists(String trustStoreName)
    {
        return configurationService.getConfigurationByName(trustStoreName).getValue().equals("not_set");
    }

}
