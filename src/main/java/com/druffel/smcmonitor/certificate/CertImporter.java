/*
 * Project: smc_cdi Package: com.sophos.mobilecontrol.server.cdi.tools.x509
 * File: CertImporter.java Created: 19.06.2013 Author: StefanGrassat (Sophos
 * Technology GmbH) Copyright: (C) 2013 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.certificate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Random;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;

import com.druffel.smcmonitor.entity.Configuration;
import com.druffel.smcmonitor.service.ConfigurationService;

/**
 * <b>Description:</b><br>
 * Utility to get information about the SSL-certificates, which are sent from a
 * server Certificates which are not trusted, can be added to the truststore
 */
@Dependent
public class CertImporter implements Serializable
{
    private static final long serialVersionUID = 5478755071538620971L;

    private static final Logger LOGGER = Logger.getLogger(CertImporter.class);

    @Inject
    private ConfigurationService configurationService;

    private static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();

    public void createNewTrustStore() throws Exception
    {
        String trustStorePassword;

        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        
        trustStorePassword = sb.toString();
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        KeyStore sslTrustStore = KeyStore.getInstance("PKCS12");
        sslTrustStore.load(null, trustStorePassword.toCharArray());
        sslTrustStore.store(baos, trustStorePassword.toCharArray()); 
        
        String encoded = new String(Base64.encodeBase64(baos.toByteArray()), StandardCharsets.UTF_8);
        configurationService.remove(configurationService.getConfigurationByName("truststore"));
        configurationService.remove(configurationService.getConfigurationByName("truststorepassword"));
        configurationService.create(new Configuration("truststore", encoded));
        configurationService.create(new Configuration("truststorepassword", trustStorePassword));
    }
    
    /**
     * Imports the ssl certificate for the given url into our trust store.
     * 
     * @param url String
     * @return <code>true</code> when new certificate has been saved, else
     *         <code>false</code>
     * @throws Exception
     * @throws CertificateImportException
     */
    public boolean importCertificateIntoTruststore(String url, String aliasSuffix) throws Exception
    {
        boolean keystoreUpdated = false;
        try
        {
            
            URL tmpUrl = new URL("https://"+url);
            if ("http".equals(tmpUrl.getProtocol()))
                return false;

            int port = tmpUrl.getPort();
            if (port == -1 && "https".equals(tmpUrl.getProtocol()))
                port = 443;

            String truststorePassword = configurationService.getConfigurationByName("truststorepassword").getValue();
            InputStream is = new ByteArrayInputStream(Base64.decodeBase64(configurationService.getConfigurationByName("truststore").getValue().getBytes()));
            KeyStore sslTrustStore = KeyStore.getInstance("PKCS12");
            sslTrustStore.load(is, truststorePassword.toCharArray());

            X509CertificateInfo[] certs = getCertDetails(tmpUrl.getHost(), port, sslTrustStore);
            for (X509CertificateInfo certInfo : certs)
            {
                if (!certInfo.isTrusted())
                {
                    addCertIntoCustomerTruststore(certInfo.getCertificate(), certInfo.getAlias() + "-" + aliasSuffix, sslTrustStore);
                    String encoded = new String(Base64.encodeBase64(sslTrustStore.toString().getBytes()), StandardCharsets.UTF_8);
                    configurationService.remove(configurationService.getConfigurationByName("truststore"));
                    configurationService.remove(configurationService.getConfigurationByName("truststorepassword"));
                    configurationService.create(new Configuration("truststore", encoded));
                    configurationService.create(new Configuration("truststorepassword", truststorePassword));
                    keystoreUpdated = true;
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception("could not get certifcates from invalid url \"" + url + "\"", e);
        }

        return keystoreUpdated;
    }

    /**
     * Connects to the host and checks the recieved SSL-certificates. The
     * information also contains, if certificates are known to the host. If not,
     * the certificates could be added with <code>addCert</code>-method to the
     * truststore.
     * 
     * @param host host
     * @param port port
     * @param trustStore trustStore
     * @return informationen about the certificates, which the host sends
     * @throws CertificateImportException
     */
    public X509CertificateInfo[] getCertDetails(String host, int port, KeyStore trustStore) throws Exception
    {
        boolean trusted;
        SSLSocketFactory factory = null;
        SavingTrustManager tm = null;
        try
        {
            SSLContext context = SSLContext.getInstance("TLS");
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);
            X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
            tm = new SavingTrustManager(defaultTrustManager);
            context.init(null, new TrustManager[] { tm }, null);
            factory = context.getSocketFactory();
        }
        catch (Exception e)
        {
            throw new Exception("could not initialize SSL context", e);
        }

        try
        {
            SSLSocket socket;

            LOGGER.info("opening connection to " + host + ":" + port + "...");
            socket = (SSLSocket) factory.createSocket(host, port);
            LOGGER.info("starting SSL handshake...");
            socket.startHandshake();
            socket.close();
            LOGGER.info("no errors, certificate is already trusted");
            trusted = true;
        }
        catch (SSLException e)
        {
            trusted = false;
            LOGGER.info("could not connect to \"" + host + ":" + port + "\", most likely untrusted");
        }
        catch (IOException e)
        {
            throw new IOException("could not ssl handshake", e);
        }

        X509Certificate[] chain = tm.chain;
        if (chain == null)
        {
            throw new IOException("could not obtain server certificate chain");
        }

        MessageDigest sha1 = null;
        try
        {
            sha1 = MessageDigest.getInstance("SHA1");
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IOException("could not get SHA1 digester", e);
        }

        LOGGER.debug("server sent " + chain.length + " certificate(s):");
        X509CertificateInfo[] ret = new X509CertificateInfo[chain.length];
        for (int i = 0; i < chain.length; i++)
        {
            X509Certificate cert = chain[i];
            LOGGER.debug(cert);
            try
            {
                sha1.reset();
                sha1.update(cert.getEncoded());
            }
            catch (CertificateEncodingException e)
            {
                LOGGER.warn("could not calculate SHA1 digest of certificate", e);
                continue;
            }
            ret[i] = new X509CertificateInfo(cert, toHexString(sha1.digest()), host + "-" + (i + 1), trusted);
        }
        return ret;
    }

    public void addCertIntoCustomerTruststore(X509Certificate cert, String alias, KeyStore trustStore) throws Exception
    {
        LOGGER.info("adding certificate to customer truststore (keystore), cert=\"" + cert + "\"");
        try
        {
            String trustStorePwd = configurationService.getConfigurationByName("truststorepassword").getValue();

            // add certificate
            trustStore.setCertificateEntry(alias, cert);

            // save trustStore in DB
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            trustStore.store(baos, trustStorePwd.toCharArray());

            String encoded = new String(Base64.encodeBase64(baos.toByteArray()), StandardCharsets.UTF_8);
            configurationService.remove(configurationService.getConfigurationByName("truststore"));
            configurationService.create(new Configuration("truststore", encoded));

            LOGGER.info("successfully added certificate to customer truststore (keystore), alias '" + alias + "'");
        }
        catch (Exception e)
        {
            throw new Exception("could not add cert and save", e);
        }
    }

    private static class SavingTrustManager implements X509TrustManager
    {
        private final X509TrustManager tm;

        private X509Certificate[] chain;

        SavingTrustManager(X509TrustManager tm)
        {
            this.tm = tm;
        }

        @Override
        public X509Certificate[] getAcceptedIssuers()
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
        {
            this.chain = chain;
            tm.checkServerTrusted(chain, authType);
        }
    }

    public static String toHexString(byte[] bytes)
    {
        StringBuilder sb = new StringBuilder(bytes.length * 3);
        for (int i = 0; i < bytes.length; i++)
        {
            int b = bytes[i];
            b &= 0xff;
            sb.append(HEXDIGITS[b >> 4]);
            sb.append(HEXDIGITS[b & 15]);
            if (i < bytes.length - 1)
                sb.append(':');
        }
        return sb.toString();
    }

    /**
     * <b>Description:</b><br>
     * Container for X.509-certificate with extended information
     *
     * @author StefanGrassat, &copy; 2013 Sophos Technology GmbH
     */
    public static class X509CertificateInfo
    {
        private X509Certificate cert;

        private String sha1;

        private String alias;

        private boolean trusted;

        public X509CertificateInfo(X509Certificate cert, String sha1, String alias, boolean trusted)
        {
            super();
            this.cert = cert;
            this.sha1 = sha1;
            this.alias = alias;
            this.trusted = trusted;
        }

        public boolean isTrusted()
        {
            return trusted;
        }

        public String getSha1()
        {
            return sha1;
        }

        public String getAlias()
        {
            return alias;
        }

        public Principal getSubjectDN()
        {
            return cert.getSubjectDN();
        }

        public Principal getIssuerDN()
        {
            return cert.getIssuerDN();
        }

        public Date getNotBefore()
        {
            return cert.getNotBefore();
        }

        public Date getNotAfter()
        {
            return cert.getNotAfter();
        }

        public X509Certificate getCertificate()
        {
            return cert;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.append("Trusted=");
            sb.append(isTrusted());
            sb.append(", Subject=");
            sb.append(getSubjectDN());
            sb.append(", Issuer=");
            sb.append(getIssuerDN());
            sb.append(", notBefore=");
            sb.append(getNotBefore());
            sb.append(", notAfter=");
            sb.append(getNotAfter());
            sb.append(", SHA1 digest=");
            sb.append(getSha1());
            return sb.toString();
        }
    }
}