/*
 * Project: monitor Package: com.druffel.smcmonitor.bean File:
 * ConfigurationManagedBean.java Created: Mar 6, 2017 Author: AmonDruffel
 * (Sophos Technology GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.event.RowEditEvent;

import com.druffel.smcmonitor.controller.ConfigurationController;
import com.druffel.smcmonitor.entity.Server;

@ManagedBean
@ViewScoped
public class ConfigurationManagedBean
{
    
    @Inject
    ConfigurationController controller;

    private String ip;
    
    private String url;
    
    private List<Server> serverList;
    
    private String description;
    
    private String hostname;
    
    private boolean useUrl;
    
    @PostConstruct
    public void init()
    {
        serverList = controller.getAllServer();
        this.useUrl = false;
    }
    
    
    public void edit(RowEditEvent event)
    {
        Server s = (Server) event.getObject();
        controller.mergeServer(s);
    }
    
    public void cancelEdit()
    {
        //empty block
    }
    
    public void addToList()
    {
        controller.persistServerToDatabase(url, ip, hostname, description, useUrl);
        serverList = controller.getAllServer();
    }
    
    
    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public List<Server> getServerList()
    {
        return serverList;
    }

    public void setServerList(List<Server> serverList)
    {
        this.serverList = serverList;
    }

    public String getDescription()
    {
        return description;
    }


    public void setDescription(String description)
    {
        this.description = description;
    }


    public boolean isUseUrl()
    {
        return useUrl;
    }


    public void setUseUrl(boolean useUrl)
    {
        this.useUrl = useUrl;
    }
    
     public String getHostname()
    {
        return hostname;
    }


    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }
    
    
}
