/*
 * Project: monitor Package: com.druffel.smcmonitor.bean File:
 * OverviewManagedBean.java Created: Mar 2, 2017 Author: AmonDruffel (Sophos
 * Technology GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.bean;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.druffel.smcmonitor.certificate.CertImporter;
import com.druffel.smcmonitor.controller.OverviewController;
import com.druffel.smcmonitor.entity.Server;

/**
 * <b>Description:</b><br>
 * Backingbean for the overview.xhtml
 *
 * @author AmonDruffel, &copy; 2017 Sophos Technology GmbH
 */
@Named
@ApplicationScoped
public class OverviewManagedBean
{

    private static final Logger log = Logger.getLogger(OverviewManagedBean.class.getName());

    @Inject
    CertImporter certImporter;

    @Inject
    OverviewController controller;

    private List<Server> serverList;

    private Date lastRefresh;

    private int intervall;

    private int refreshedAgo;

    private boolean newBootup;

    private String username;

    private String password;

    private String customer;

    private boolean refreshing;

    @PostConstruct
    public void init()
    {
        serverList = controller.getAllServer();

        newBootup = true;
        refreshing = false;

        refreshedAgo = 0;
        setLastRefresh(new Date());

    }

    public void saveLoginData()
    {
        newBootup = false;
    }

    public void resetRefreshedAgo()
    {
        refreshedAgo = controller.getLastRefreshInMinutes();
    }

    public void addGrowlMessage(String title, String body)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(title, body));
    }

    public void growlStart()
    {
        addGrowlMessage("Wird aktualisiert..", "Bitte warten");
    }

    public void refreshPage() throws IOException
    {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    public void reload() throws Exception
    {

        if (refreshing)
        {
            addGrowlMessage("Wird bereits Aktualisiert!", "Bitte warten.");
            return;
        }
        else
        {
            refreshing = true;
            serverList = controller.getAllServer();

            serverList = controller.refreshServerInformation(serverList, username, password, customer);
            addGrowlMessage("Wurde aktualisiert", new Date().toString());
            refreshing = false;
        }
    }

    public List<Server> getServerList()
    {
        return serverList;
    }

    public void setServerList(List<Server> serverList)
    {
        this.serverList = serverList;
    }

    public Date getLastRefresh()
    {
        log.info("Getting last refresh time");
        return lastRefresh;
    }

    public void setLastRefresh(Date lastRefresh)
    {
        this.lastRefresh = lastRefresh;
    }

    public int getIntervall()
    {
        log.info("Getting intervall configuration");
        return intervall;
    }

    public void setIntervall(int intervall)
    {
        this.intervall = intervall;
    }

    public static Logger getLog()
    {
        return log;
    }

    public int getRefreshedAgo()
    {

        long sec = (new Date().getTime() - lastRefresh.getTime()) / 1000;
        long minutes = sec / 60;
        refreshedAgo = (int) minutes;

        return refreshedAgo;
    }

    public void setRefreshedAgo(int refreshedAgo)
    {
        this.refreshedAgo = refreshedAgo;
    }

    public boolean isNewBootup()
    {
        return newBootup;
    }

    public void setNewBootup(boolean newBootup)
    {
        this.newBootup = newBootup;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getCustomer()
    {
        return customer;
    }

    public void setCustomer(String customer)
    {
        this.customer = customer;
    }

    public boolean isRefreshing()
    {
        return refreshing;
    }

    public void setRefreshing(boolean refreshing)
    {
        this.refreshing = refreshing;
    }

}
