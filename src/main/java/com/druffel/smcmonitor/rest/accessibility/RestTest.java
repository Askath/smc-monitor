/*
 * Project: monitor Package: com.druffel.smcmonitor.rest.accessibility File:
 * RestTest.java Created: Apr 19, 2017 Author: AmonDruffel (Sophos Technology
 * GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.rest.accessibility;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.druffel.smcmonitor.rest.response.ErrorResponse;

public class RestTest
{

    public static ErrorResponse testAccessibility(String webserviceUri)
    {

        ErrorResponse errorResponse = new ErrorResponse();
        
        try
        {
            String targetUri = "https://" + webserviceUri + "/rs/test/";
            
            ResteasyClient client = new ResteasyClientBuilder().build();
            ResteasyWebTarget target = client.target(targetUri);
            
            Response response = target.request(MediaType.APPLICATION_JSON).get();
            
            System.out.println("response: " + response.readEntity(String.class));
            
            response.close();
            client.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return errorResponse;

    }

}
