/*
 * Project: monitor Package: com.druffel.smcmonitor.rest.login File:
 * RestVersion.java Created: Apr 4, 2017 Author: AmonDruffel (Sophos Technology
 * GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.rest.version;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.druffel.smcmonitor.rest.response.VersionRespond;

public class RestVersion
{
    
    

    public static String resGetSMCVersion(String webserviceUri, String authToken)
    {

        VersionRespond versionRespond = new VersionRespond();
        
        try
        {

            String targetUri = "https://" + webserviceUri + "/rs/meta/version";

            ResteasyClient client = new ResteasyClientBuilder().build();
            ResteasyWebTarget target = client.target(targetUri);

            Response response = target.request(MediaType.APPLICATION_JSON).header("X-SMCRS-Auth-Session", authToken).get();

            ObjectMapper mapper = new ObjectMapper();
            versionRespond = mapper.readValue(response.readEntity(String.class), VersionRespond.class);

            response.close();
            client.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return versionRespond.getVersion();
    }

}
