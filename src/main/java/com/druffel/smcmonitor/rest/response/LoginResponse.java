/*
 * Project:		monitor
 * Package:		com.druffel.smcmonitor.rest.login
 * File: 		LoginResponse.java
 *
 * Created:		Apr 4, 2017
 * Author:		AmonDruffel (Sophos Technology GmbH)
 * Copyright:	(C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.rest.response;

import java.util.List;

public class LoginResponse
{

    private String userName;
    private String authToken;
    private String loginDate;
    private List<String> rights;
    
    public LoginResponse()
    {
        
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getAuthToken()
    {
        return authToken;
    }

    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public String getLoginDate()
    {
        return loginDate;
    }

    public void setLoginDate(String loginDate)
    {
        this.loginDate = loginDate;
    }

    public List<String> getRights()
    {
        return rights;
    }

    public void setRights(List<String> rights)
    {
        this.rights = rights;
    }
    
}
