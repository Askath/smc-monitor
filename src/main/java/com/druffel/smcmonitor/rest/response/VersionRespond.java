/*
 * Project:		monitor
 * Package:		com.druffel.smcmonitor.rest.response
 * File: 		VersionRespond.java
 *
 * Created:		Apr 4, 2017
 * Author:		AmonDruffel (Sophos Technology GmbH)
 * Copyright:	(C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.rest.response;

public class VersionRespond
{

    private String Version;

    public String getVersion()
    {
        return Version;
    }

    public void setVersion(String version)
    {
        Version = version;
    }
    
            
}
