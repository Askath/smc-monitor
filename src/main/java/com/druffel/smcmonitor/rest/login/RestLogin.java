/*
 * Project: monitor Package: com.druffel.smcmonitor.rest File: RestLogin.java
 * Created: Apr 4, 2017 Author: AmonDruffel (Sophos Technology GmbH) Copyright:
 * (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.rest.login;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.druffel.smcmonitor.rest.response.LoginResponse;

public class RestLogin
{

    
    public static LoginResponse rsLogin(String webserviceURI, String user, String password, String costumer)
    {
        final Logger log = Logger.getLogger(RestLogin.class.getName());
        
        LoginResponse loginResponse = new LoginResponse();
        try
        {

            String targetUri = "https://" + webserviceURI + "/rs/login/";

            ResteasyClient client = new ResteasyClientBuilder().build();
            ResteasyWebTarget target = client.target(targetUri);

            // Form for login data
            Form loginForm = new Form().param("customer", costumer).param("user", user).param("password", password);

            Response response = target.request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(loginForm, MediaType.APPLICATION_FORM_URLENCODED_TYPE.withCharset("utf-8")));
            String jsonResponse = response.readEntity(String.class);

            
            if(response.getStatus() == 401)
            {
                return null;
            }
            
            log.info("Server:" + targetUri + "\n" + jsonResponse + "\nStatus: " + response.getStatus());

            ObjectMapper mapper = new ObjectMapper();

            loginResponse = mapper.readValue(jsonResponse, LoginResponse.class);

            
            response.close();
            client.close();
        }
        catch (Exception e)
        {
            log.info("Can't recieve login response. Check certificates for " + webserviceURI + " and login data for your webservice access");
            log.log(Level.SEVERE, e.getMessage());
            loginResponse = null;
            return loginResponse;
        }
        return loginResponse;

    }

    public static int resLogout(String webserviceUri, String authToken)
    {

        String targetUri = "https://" + webserviceUri + "/rs/logout";

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(targetUri);

        Response response = target.request(MediaType.MEDIA_TYPE_WILDCARD).header("X-SMCRS-Auth-Session", authToken)
                .post(Entity.entity("", MediaType.APPLICATION_FORM_URLENCODED));

        response.close();
        client.close();

        return response.getStatus();
    }
}
