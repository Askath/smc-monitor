/*
 * Project:		monitor
 * Package:		com.druffel.smcmonitor.network
 * File: 		ServerChecker.java
 *
 * Created:		May 16, 2017
 * Author:		AmonDruffel (Sophos Technology GmbH)
 * Copyright:	(C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.network;

import javax.enterprise.context.Dependent;

import com.druffel.smcmonitor.rest.login.RestLogin;
import com.druffel.smcmonitor.rest.response.LoginResponse;
import com.druffel.smcmonitor.rest.version.RestVersion;

@Dependent
public class ServerChecker
{

    public LoginResponse getLoginResponse(String connectionAdress, String username, String password, String costumer)
    {
        return RestLogin.rsLogin(connectionAdress, username, password, costumer);
    }
    
    

    public String getVersion(String connectionUrl, LoginResponse response)
    {
        String version = RestVersion.resGetSMCVersion(connectionUrl, response.getAuthToken());
        RestLogin.resLogout(connectionUrl, response.getAuthToken());

        return version;
    }
}
