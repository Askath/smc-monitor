/*
 * Project: monitor Package: com.druffel.smcmonitor.entity File:
 * Configuration.java Created: Mar 7, 2017 Author: AmonDruffel (Sophos
 * Technology GmbH) Copyright: (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "configuration", catalog = "monitor")
public class Configuration implements Serializable
{

    /**
     * <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "configurationid", unique = true, nullable = false)
    private int configurationid;

    @Column(name = "confkey", length = 50)
    private String key;

    @Column(name = "confvalue")
    private String value;
    
    public Configuration()
    {
        //default constructer
    }
    
    public Configuration(String key, String value)
    {
        this.key = key;
        this.value = value;
    }

    public int getConfigurationid()
    {
        return configurationid;
    }

    public void setConfigurationid(int configurationid)
    {
        this.configurationid = configurationid;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

}
