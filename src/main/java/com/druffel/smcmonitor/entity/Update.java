/*
 * Project: monitor Package: com.druffel.smcmonitor.entity File: Update.java
 * Created: Mar 9, 2017 Author: AmonDruffel (Sophos Technology GmbH) Copyright:
 * (C) 2017 Sophos Technology GmbH
 */
package com.druffel.smcmonitor.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "update", catalog = "monitor")
public class Update
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "updateid", unique = true, nullable = false)
    private int updateid;
    
    @Column(name = "updated", nullable = false)
    private Date updated;

    public Update()
    {
        // default constructer
    }

    public Update(Date updated)
    {
        this.updated = updated;
    }

    public Update(int updateid, Date updated)
    {
        this.updated = updated;
        this.updateid = updateid;
    }
    
    public Date getUpdated()
    {
        return updated;
    }

    public void setUpdated(Date updated)
    {
        this.updated = updated;
    }

}
