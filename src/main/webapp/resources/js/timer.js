
var timeinterval;
var timer;
var end;

function getTimeRemaining() 
{
	var t = Date.parse(end) - Date.parse(new Date());
	var seconds = Math.floor((t/1000) % 60);
	var minutes = Math.floor((t/1000/60) % 60);
	return {
		'seconds': seconds,
		'minutes': minutes,
		'total': t
	};
}

function initializeClock(id, endtime)
{	timeinterval = setInterval(updateClock, 1000);
	end = endtime;
	updateClock(id);
}

function updateClock(id)
{
	var t = getTimeRemaining();	
	console.log(t.total + "");
	if(t.total<=0){
		try {
			clearInterval(timeinterval);
			refresh();
		}
		catch(e)
		{
			//nothing
		}
	}
}